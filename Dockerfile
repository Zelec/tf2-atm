# Docker-TF2-ATM, by Isaac Towns <Zelec@timeguard.ca>

# Rather than making the git repo a submodule of this repo, it would be better off to change the docker file as needed.
# FROM alpine/git:1.0.7 AS repo
# Change target release tag here

# Normally alpine/git uses /git for it's workdir, however it's declared as a volume in the upstream resource
# so it has a hard time keeping data intact inbetween layers.
# RUN mkdir /app
# WORKDIR /app
# RUN git clone https://gitlab.com/Zelec/tf2-atm.git .
    # && \
    # git checkout tags/${RELEASE}

# for Python builds, in my testing Alpine has more issues and is slower to 
# build compared to the Debian images for Python.
FROM library/python:3.8.1-buster AS docker-tf2-atm
LABEL maintainer Isaac Towns <Zelec@timeguard.ca>
# Pull repo data from git container
#COPY --from=repo /app/ /app
# Following 3 lines help with keeping container base the same for docker
# So while developing this app, you don't need to redownload steampy every time
COPY requirements.txt /tmp/
COPY docker-entrypoint.sh /
RUN \
    apt-get update && apt-get upgrade -y && \
    apt-get install -y dumb-init && \
    mkdir /app && \
    pip install --no-cache-dir -U -r /tmp/requirements.txt && \
    rm -rf /tmp/* /var/tmp/* /var/lib/apt/lists/*
WORKDIR /app
COPY . /app/
# Installs updates, Moves config outside of /app, & setup unpriviledged user 
# (Adapted from Linuxserver.io base containers)
VOLUME ["/home/user"]
RUN \
    rm -rf ./.git && \
    mkdir -p /defaults /logs /config /home/user && \
    ln -s /config /app/config && \
    ln -s /logs /app/logs && \
    useradd -u 911 -U -d /home/user -s /bin/false user && \
    usermod -G users user
# Dependency install
 
VOLUME ["/config"]
ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/docker-entrypoint.sh"]