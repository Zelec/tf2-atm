#!/usr/bin/env python3
# Created by Isaac Towns <Zelec@timeguard.ca>

# ConfigParser for easy ini files
import sys
import configparser
import json
import logging

# Config file locations
configFile = "config/config.ini"
botFile = "config/bots.ini"

# Main config declaration
mainConf = configparser.ConfigParser()
mainConf.read(configFile)
# Bot config declaration
botConf = configparser.ConfigParser()
botConf.read(botFile)

# Creates example main config with some sane defaults for Metal & regular Mann Co Supply Crate Keys when called
def createConf():
    logging.warning("config.ini either empty, or doesn't exist, creating example config.")
    mainConf["Main"] = {}
    mainConf["Items"] = {}
    mainConf["Main"]["Owner_Steam_ID"] = "CHANGEME"
    mainConf["Main"]["Steam_Web_Api_Key"] = "CHANGEME"
    mainConf["Main"]["Trading_Bot_Steam_ID"] = "CHANGEME"
    mainConf["Main"]["Time_Interval_Seconds"] = "1200"
    mainConf["Items"]["5021"] = "50"
    mainConf["Items"]["5002"] = "1000"    
    with open(configFile, "w") as configOut:
        mainConf.write(configOut)

# Creates template bot config when called
def createBotConf():
    logging.warning("Warning: bots.ini either empty, or doesn't exist, creating example config.")
    botConf["ExampleBot"] = {}
    botConf["ExampleBot"]["Steam_ID_64"] = "SteamID64"
    # The 4 pieces of info needed to login to steam with no user interaction.
    botConf["ExampleBot"]["Steam_Username"] = "examplebot"
    botConf["ExampleBot"]["Steam_Password"] = "123456789"
    botConf["ExampleBot"]["Shared_Secret"] = "SS"
    botConf["ExampleBot"]["Identity_Secret"] = "IS"
    botConf["ExampleBot"]["Master"] = "False"
    with open(botFile, "w") as configOut:
        botConf.write(configOut)

# Checks config of the main config and bot config
# Checks in main config for Skip-Check, Confirm. If exists then skip the whole thing.
# Otherwise check the config for first time misedits and set confNeedsFixing var to 1 if errors are found
def checkConf():
    if mainConf.has_option("Skip-Check", "Confirm") == True:
        logging.info("Skip config check declared in config.ini")
    else:
        mainEmpty = len(mainConf.sections())
        botEmpty = len(botConf.sections())
        confNeedsFixing = 0
        if mainEmpty == 0:
            createConf()
        if botEmpty == 0:
            createBotConf()
        if  mainConf["Main"]["Owner_Steam_ID"] == "CHANGEME" or mainConf["Main"]["Steam_Web_Api_Key"] == "CHANGEME" or mainConf["Main"]["Trading_Bot_Steam_ID"] == "CHANGEME":
            logging.error("Error: Main config is set incorrectly")
            confNeedsFixing = 1
        if botConf.has_section("ExampleBot") == True:
            logging.error("Error: Bot config has example bot config in it.")
            confNeedsFixing = 1
        if confNeedsFixing == 1:
            sys.exit(1)

# Simple config grab delcarations
def getKey(section, subsection):
    return mainConf[section][subsection]
def getSection(section):
    return mainConf[section]
def getBotKey(section, subsection):
    return botConf[section][subsection]
def listBots():
    return botConf.sections()

def getBotSteamGuardJSON(botName):
    steamGuard = {}
    steamID = getBotKey(botName, "steam_id")
    sharedSecret = getBotKey(botName, "shared_secret")
    identitySecret = getBotKey(botName, "identity_secret")
    steamGuard["steamid"] = steamID
    steamGuard["shared_secret"] = sharedSecret
    steamGuard["identity_secret"] = identitySecret
    logging.debug("Constructed and returning Steamguard file for " + botName)
    return json.dumps(steamGuard)

def getMasterBot():
    botName = "NULL"
    for bot in botConf.sections():
        if botConf.has_option(bot, "Master") == True:
            if botConf[bot]["Master"] == "True":
                botName = bot
                break
    if botName == "NULL":
        print("Error: No bot defined as master")
        sys.exit(1)
    logging.debug("Returned Master Bot Section Name " + botName)
    return botName

def getMasterBotID():
    botID = "NULL"
    for bot in botConf.sections():
        if botConf.has_option(bot, "Master") == True:
            if botConf[bot]["Master"] == "True":
                botID = botConf[bot]["Steam_ID"]
                break
    if botID == "NULL":
        print("Error: No bot defined as master")
        sys.exit(1)
    logging.debug("Returned Master Bot Steam ID " + str(botID))
    return botID

def getDebugFlag():
    return mainConf.has_option("Main", "Debug")