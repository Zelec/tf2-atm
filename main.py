#!/usr/bin/env python3
import lib.conf as conf
import sys
import time
import logging
import json
from steampy.client import SteamClient, Asset
from steampy.utils import GameOptions
from logging.handlers import TimedRotatingFileHandler

# Logging setup
log = logging.getLogger()
log.setLevel(logging.DEBUG)
fileHandler = TimedRotatingFileHandler("logs/tf2-atm.log", when="midnight", interval=1, backupCount=5)
streamHandler = logging.StreamHandler(sys.stdout)
if conf.getDebugFlag() == True:
    fileHandler.setLevel(logging.DEBUG)
    streamHandler.setLevel(logging.DEBUG)
else:
    fileHandler.setLevel(logging.INFO)
    streamHandler.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s:%(levelname)s:%(message)s")
streamHandler.setFormatter(formatter)
fileHandler.setFormatter(formatter)
log.addHandler(fileHandler)
log.addHandler(streamHandler)

# Global variable mess
log.info("Starting up tf2-atm...")
log.info("Checking config for errors...")
conf.checkConf()
steamAPIKey = conf.getKey("Main", "steam_web_api_key")
steam_client = SteamClient(steamAPIKey)
owner = conf.getKey("Main", "Owner_Steam_ID")
game = GameOptions.TF2
masterBot = conf.getMasterBot()
masterBotID = conf.getMasterBotID()
tradingBotID = conf.getKey("Main", "Trading_Bot_Steam_ID")
timeInterval = int(conf.getKey("Main", "Time_Interval_Seconds"))
bankedItems = conf.getSection("Items")
tradingBotParams = {"key": steamAPIKey, "steamid": tradingBotID}
masterBotParams = {"key": steamAPIKey, "steamid": masterBotID}

def masterBotThread():
    # Logs in master bot
    outboundTradeBuffer = list()
    inboundTradeBuffer = list()
    username = conf.getBotKey(masterBot, "steam_username")
    password = conf.getBotKey(masterBot, "steam_password")
    steamGuard = conf.getBotSteamGuardJSON(masterBot)
    steam_client.login(username, password, steamGuard)
    log.debug("Master bot logged in.")
    # Clear all trades before we start
    cancelTradeOffers(steam_client)
    # Global variables here so writes/changes to these effect globally
    while True:
        # Incase the bot ever logs out on us for some reason
        while steam_client.is_session_alive() == False:
            steam_client.login(username, password, steamGuard)
        # Clears trade buffers between runs
        outboundTradeBuffer.clear()
        inboundTradeBuffer.clear()
        # Function to compile a whole trade
        fullTrade = externalTradeCompiler()
        if "outboundTradeBuffer" in fullTrade:
            for assetid in fullTrade["outboundTradeBuffer"]:
                outboundTradeBuffer.append(Asset(str(assetid), game))
        if "inboundTradeBuffer" in fullTrade:
            for assetid in fullTrade["inboundTradeBuffer"]:
                inboundTradeBuffer.append(Asset(str(assetid), game))
        if conf.getDebugFlag() == True:
            log.debug("Debug mode enabled, not sending trade")
            log.debug("Inbound trade buffer count:")
            log.debug(len(inboundTradeBuffer))
            log.debug("Outbound trade buffer count:")
            log.debug(len(outboundTradeBuffer))
        else:
            if len(inboundTradeBuffer) == 0 and len(outboundTradeBuffer) == 0:
                log.warning("Nothing can be traded, check configured limits, or check that your trading bot is running properly.")
            else:
                log.info("We will send " + str(len(outboundTradeBuffer)) + " items, and receive " + str(len(inboundTradeBuffer)) + " items.")
                trade = steam_client.make_offer(outboundTradeBuffer, inboundTradeBuffer, tradingBotID, "TF2-ATM Trade")
                log.debug(trade)
                if "tradeofferid" in trade:
                    log.info("Offer sent")
                else:
                    log.error(trade)
                    log.error("Sending trade offer failed")
        if conf.getDebugFlag() == True:
            log.debug("Debug flag set, logging out of account and exiting")
            steam_client.logout()
            sys.exit(0)
        else:
            log.info("Sleeping for " + str(timeInterval) + " seconds before continuing")
            time.sleep(timeInterval)

def cancelTradeOffers(steamClient):
    log.info("Canceling/Declining all currently available trades on boot")
    tradeOffers = steamClient.get_trade_offers(False)
    try:
        for trades in tradeOffers["response"]["trade_offers_received"]:
            log.debug("Declining inbound offer " + str(trades["tradeofferid"]))
            steamClient.decline_trade_offer(trades["tradeofferid"])
    except:
        log.debug("No inbound offers to cancel")

    try:
        for trades in tradeOffers["response"]["trade_offers_sent"]:
            log.debug("Canceling outbound offer " + str(trades["tradeofferid"]))
            steamClient.cancel_trade_offer(trades["tradeofferid"])
    except:
        log.debug("No outbound offers to cancel")

# Shared function to make a list usable for trading
def tradeAppend(defindex, difference, inventory):
    tradeBuffer = list()
    for item in inventory["result"]["items"]:
        if int(item["defindex"]) == int(defindex) and difference > 0:
            tradeBuffer.append(str(item["id"]))
            difference = difference - 1
    return tradeBuffer

def itemGrabber(defindex, confLimit, invInternal, invExternal):
    log.debug("Defindex:")
    log.debug(defindex)
    # Base setup to be sure nothing is undeclared
    tradeBuffer = dict()
    invIntList = list()
    invExtList = list()
    inbCount = 0
    outCount = 0
    inbLimit = 0
    outLimit = 0
    # Checks External inventory for items matching the provided defindex
    for index in invExternal["result"]["items"]:
        if int(index["defindex"]) == int(defindex):
            inbCount = inbCount + 1
    # Checks Internal inventory for items matching the provided defindex
    for index in invInternal["result"]["items"]:
        if int(index["defindex"]) == int(defindex):
            outCount = outCount + 1
    # Debug reporting
    log.debug("External/Inbound count:")
    log.debug(inbCount)
    log.debug("Internal/Outbound count:")
    log.debug(outCount)
    # If confLimit is set to 0, no need to evaluate the limits, take it all in from the bot
    if confLimit == 0:
        log.debug("Limit is set to zero, pulling all items, sending none back")
        inbLimit = inbCount
        outLimit = 0
    # Simple if checking to set inbound and outbound limits accordingly (Avoids item counting never reaching 0 loop errors)
    else:
        if inbCount > confLimit:
            log.debug("External inv count is higher than limit, pulling extra items")
            inbLimit = inbCount - confLimit
        if inbCount < confLimit:
            log.debug("External inv count is lower than limit, sending items to make up difference")
            outLimit = confLimit - inbCount
        # If our outbound limit is greater than what we can provide, then send what we can.
        if outLimit > outCount:
            log.debug("Bot doesn't have enough items to send, setting count to equal limit")
            outLimit = outCount
    # More debug reporting
    log.debug("Inbound limit:")
    log.debug(inbLimit)
    log.debug("Outbound limit:")
    log.debug(outLimit)
    # Call TradeAppend function to make the needed lists
    for item in tradeAppend(defindex, outLimit, invInternal):
        invIntList.append(item)
    for item in tradeAppend(defindex, inbLimit, invExternal):
        invExtList.append(item)
    # Wraps the list into a Dictionary so both can be return externally.
    tradeBuffer["invIntList"] = invIntList.copy()
    tradeBuffer["invExtList"] = invExtList.copy()
    return tradeBuffer



# Returns how much space is left in the inventory
def getInvSpaceLeft(inventory):
    result = inventory["result"]["num_backpack_slots"] - len(inventory["result"]["items"])
    return result

# Returns how many spaces are used in the inventory
def getInvSpaceUsage(inventory):
    result = len(inventory["result"]["items"])
    return result

# Returns how many spaces total are available to the account (AKA How many backpack expanders were used)
def getInvSpace(inventory):
    result = inventory["result"]["num_backpack_slots"]
    return result

# Pulls inventory by taking steam client class, the API key, and steamid and calls the webapi itself.
# I use the direct API call instead of steampy's native call because it doesn't show the defindex value for tf2 items at all.
def getInventory(steamClient, apiKey, steamID):
    parameters = {"key": str(apiKey), "steamid": str(steamID)}
    sleepTime = 5
    requestInventory = steamClient.api_call("GET", "IEconItems_440", "GetPlayerItems", "v1", parameters)
    # Get requests response, and don't proceed unless we get back a http status code 200, waiting 5 seconds each time inbetween
    while requestInventory.status_code != 200:
        log.info("Failed to pull inventory for SteamID " + str(steamID) + ", Sleeping for " + str(sleepTime) + " Seconds.")
        time.sleep(sleepTime)
        requestInventory = steamClient.api_call("GET", "IEconItems_440", "GetPlayerItems", "v1", parameters)
        sleepTime = sleepTime + 5
    return requestInventory

# The meat of the main program
def externalTradeCompiler():
    finalTradeBuffer = dict()
    outboundTradeBuffer = list()
    inboundTradeBuffer = list()
    # Pulling inventories
    log.debug("Pulling Master Bot Inventory")
    getMasterBotInv = getInventory(steam_client, steamAPIKey, masterBotID).json()
    getTradingBotInv = getInventory(steam_client, steamAPIKey,tradingBotID).json()
    with open("config/masterbot.json", "w") as f:
        json.dump(getMasterBotInv, f)
    with open("config/tradingbot.json", "w") as f:
        json.dump(getTradingBotInv, f)
    # Checks how much free space both bots have
    masterBotInvLimits = getInvSpaceLeft(getMasterBotInv)
    tradingBotInvLimits = getInvSpaceLeft(getTradingBotInv)
    log.debug("I can fit " + str(masterBotInvLimits) + " more items in my inventory")
    log.debug("External trading bot can fit " + str(tradingBotInvLimits) + " more items in their inventory")
    # Checks for all items listed in config.ini under Items
    log.debug("Checking Def_index skus in config.ini...")
    for defindex in bankedItems:
        # Pulls limit for defindex
        # Difference counting, making sure each inventory has enough before trying to for-loop a difference var that can never reach 0
        defindexList = itemGrabber(defindex, int(conf.getKey("Items", defindex)), getMasterBotInv, getTradingBotInv)
        if "invIntList" in defindexList:
            for assetid in defindexList["invIntList"]:
                outboundTradeBuffer.append(assetid)
        if "invExtList" in defindexList:
            for assetid in defindexList["invExtList"]:
                inboundTradeBuffer.append(assetid)
        
    # Sanity checking
    if getInvSpaceUsage(getMasterBotInv) >= getInvSpace(getMasterBotInv) - 5:
        # If our inventory is at capacity, don't take any more items.
        log.warning("Our inventory is too full, not taking any more items from external sources")
        inboundTradeBuffer.clear()            
    if len(inboundTradeBuffer) >= masterBotInvLimits and len(inboundTradeBuffer) != 0:
        # tf2-atm, doesn't matter if bot is stuffed, as long as external bot can still trade
        log.warning("Not enough space in my backpack to fit all items, reducing inbound list")
        while len(inboundTradeBuffer) >= masterBotInvLimits:
            inboundTradeBuffer.pop()
    if len(outboundTradeBuffer) >= tradingBotInvLimits:
        # If external bot can't accept the trade at all, don't stuff the inventory with more items, better to not cause more issues
        log.warning("Not enough space in external trading bot's backpack, clearing outbound list")
        outboundTradeBuffer.clear()
    

    # For putting everything into one dictionary for return values.
    finalTradeBuffer["inboundTradeBuffer"] = inboundTradeBuffer.copy()
    finalTradeBuffer["outboundTradeBuffer"] = outboundTradeBuffer.copy()
    return finalTradeBuffer
# To have the whole script read and call-able before calling any functions
if __name__ == "__main__":
    masterBotThread()
    
