#!/usr/bin/dumb-init /bin/bash

# Ripped from Linuxserver.io baseimages.
PUID=${PUID:-911}
PGID=${PGID:-911}

groupmod -o -g "$PGID" user
usermod -o -u "$PUID" user
chown -R user:user /app
chown -R user:user /config
chown -R user:user /defaults
chown -R user:user /logs

find /config -type d -exec chmod 700 {} \;
find /config -type f -exec chmod 600 {} \;

find /logs -type d -exec chmod 700 {} \;
find /logs -type f -exec chmod 600 {} \;

su -l -s /bin/bash -c "cd /app && python3 /app/main.py" user