# tf2-atm

TF2 Storage network for currencies and overstocked items

## To run code with docker:
* `docker-compose build`
* `docker-compose run --rm tf2-atm`

This will build and run the code, then delete the container after the code is finished

After code edits, both commands need to be run again.

## Installing tf2-atm with Docker

The recommended way is to use a linux machine running docker

Here is an example docker-compose.yml to use
```yaml
version: '2'
services:
  tf2-atm:
    image: zelec/tf2-atm:latest
    environment:
      - PUID=1000
      - PGID=1000
    restart: unless-stopped
    volumes: 
      - ./config:/config
      - ./logs:/logs
```

## Installing tf2-atm manually

First, clone this repo:  
`git clone https://gitlab.com/zelec/tf2-atm.git`

cd into tf2-atm and run this command after:  
`cd tf2-atm`  
`pip install -U -r requirements.txt`

This will install the required dependencies for this program

Finally run the program:  
`python ./main.py`


## Configuring tf2-atm

The first time it will error out because it's generated the needed config files in `./config` and will need to be changed.

config.ini
```
[Main]
owner_steam_id = CHANGEME
steam_web_api_key = CHANGEME
trading_bot_steam_id = CHANGEME
time_interval_seconds = 300

[Items]
5000 = 0
5001 = 0
5002 = 1000
```

At the very least you will have to change every `CHANGEME` to be proper (All steamID's are expected to be SteamID64)

To get a steam web api key, go to [here](https://steamcommunity.com/dev/apikey)

Items is your overstock variables, the 3 placed in by default are the defindexes for Metal. The priority is set via how high it is on the list.  
(By the default above, it will pull all Scrap Metal, all Reclaimed Metal, and keep the trading bot's stock of Refined Metal at 1000)

If you wanted to auto resupply keys, you can add in defindex 5021 for example

bots.ini:  
```
[ExampleBot]
steam_username = examplebot
steam_password = 123456789
steam_id_64 = SteamID64
shared_secret = SS
identity_secret = IS
master = False
```

Change Examplebot's section name to anything other than ExampleBot

Fill out all the details.

This will require auto logging in and auto confirmation of trades, so it would be best to use [Steam Desktop Authenticator](https://github.com/Jessecar96/SteamDesktopAuthenticator) to get the maFile for your bot's account containing their SteamGuard secrets.

Finally change master to True, eventually this program will allow you to run multiple bots at once, and the master designation is the in and out point for the storage cluster.

But the bot will currently output an error trying to find the master bot if this isn't set.

To get the bot automatically trading with tf2-automatic for example, set the master bot steamid as an admin in the tf2-automatic config.

## Why this exists
This bot reduces the costs for running multiple tf2 trading bots

From start to finish in USD:  
- 15 days to get around trade ban from setting up Steam Guard Auth
- Mobile Phone Number (To setup steam guard auth, can be reused multiple times)
- Tf2 premium upgrade, requires buying one item from the mann co store
  - Recommended to buy atleast 5 USD worth of items to get the Steam account unlimited too. (tf2 upgrade to premium item satisfies this requirement)
  - Optional but recommended, 27 backpack expanders, $26.73 from mann co store. (Can be used instead of tf2 premium upgrade aswell since it satifies the $5 USD minimum for steam) expands up to 3000 slots with 27 total.
    - Can be had for cheaper from Marketplace.

- Optional Donating to backpack.tf, allows for more classified listings, and allows autobumping to be on backpack.tf's end instead of the bot $35 USD for 1 hour autobumping.
  - Can also be done as $5USD per month for backpack.tf premium (Allows autobumping every 30 mins)

So in total to get a bot setup with no inventory is 15 days and 5 USD.
$61.73 - $66.73 USD for a fully furnished bot (Either all at once, or over time) with 1 hour autobumping from backpack.tf donations

Then $5 USD per month if you want backpack.tf premium

That is for one public facing bot. For getting the master bot running for tf2-atm it would just cost 15 days and $5 USD  
or at most $26.73 - $31.73 USD for a full bot

So no monthly or yearly costs beyond having a system run this program 24/7. Way cheaper than setting up a fleet of public trading bots.